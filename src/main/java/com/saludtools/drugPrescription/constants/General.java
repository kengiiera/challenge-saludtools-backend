package com.saludtools.drugPrescription.constants;

/**
 * @author kramos
 */
public final class General {

    public static final String MSG_ERROR = "OCURRIÓ UN ERROR, POR FAVOR INFORME AL ADMINISTRADOR";
    public static final String MSG_FOUND = "INFORMACIÓN ENCONTRADA CON ÉXITO";
    public static final String MSG_NOT_FOUND = "NO HAY REGISTROS";
    public static final String MSG_SAVE = "INFORMACIÓN GUARDADA CON EXITO";
    public static final String MSG_UPD = "INFORMACIÓN ACTUALIZADA CON EXITO";
    public static final String MSG_DELETE = "INFORMACIÓN ELIMINADA CON EXITO";
    public static final String MSG_ERROR_TRANS = "OCURRIÓ UN ERROR, POR FAVOR REVISE LA INFORMACIÓN";
    public static final String MSG_NOT_FOUND_ID = "REGISTRO NO ENCONTRADO";
}
