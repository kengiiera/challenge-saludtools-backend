package com.saludtools.drugPrescription.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Modelo de respuesta
 *
 * @author kramos
 * @version 1
 * @since 22/01/2023
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponse<T> {
    private int status;
    private String message;
    private Object result;

}
