package com.saludtools.drugPrescription.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.util.Date;

/**
 * Entidad de prescripcion de medicamentos
 *
 * @author kramos
 * @version 1
 * @since 22/01/2023
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "saludtools", name = "prescripcion_medicamento")
public class DrugPrescription {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_prescripcion_medicamento")
    @SequenceGenerator(name = "generator_prescripcion_medicamento",
            schema = "saludtools",
            sequenceName = "seq_prescripcion_medicamento", allocationSize = 1)
    @Column(name = "id")
    private BigInteger id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombre")
    private String name;
    @JoinColumn(name = "patient_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Patient patient;
    @Basic(optional = false)
    @NotNull
    @Column(name = "medicamento_id")
    private BigInteger drugId;
    @Basic(optional = false)
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm", timezone = "America/Bogota")
    @Column(name = "fecha_creacion")
    private Date dateInsert;

}
