package com.saludtools.drugPrescription.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.util.Date;

/**
 * Entidad de pacientes
 *
 * @author kramos
 * @version 1
 * @since 22/01/2023
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "saludtools", name = "paciente")
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_paciente")
    @SequenceGenerator(name = "generator_paciente",
            schema = "saludtools",
            sequenceName = "seq_paciente", allocationSize = 1)
    @Column(name = "id")
    private BigInteger id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 99)
    @Column(name = "nombre")
    private String name;
    @Basic(optional = false)
    @Size(min = 1, max = 99)
    @Column(name = "apellido")
    private String surname;
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Basic(optional = false)
    @Column(name = "fecha_nacimiento")
    private Date dateBirth;
    @Basic(optional = false)
    @Column(name = "estado")
    private Boolean status;
    @Column(name = "genero")
    private String gender;

}
