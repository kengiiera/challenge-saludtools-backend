package com.saludtools.drugPrescription.controller;

import com.saludtools.drugPrescription.dto.DrugPrescriptionDTO;
import com.saludtools.drugPrescription.dto.DrugPrescriptionDTOResponsePaginate;
import com.saludtools.drugPrescription.model.ApiResponse;
import com.saludtools.drugPrescription.service.DrugPrescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigInteger;

import static com.saludtools.drugPrescription.constants.General.*;

/**
 * Controlador de prescripcion de medicamentos
 *
 * @author kramos
 * @version 1
 * @since 22/01/2023
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("drug-prescription")
public class DrugPrescriptionController {


    private final DrugPrescriptionService drugPrescriptionService;

    /**
     * Constructor del controlador
     *
     * @param drugPrescriptionService servicio de prescripcion de medicamentos
     */
    @Autowired
    public DrugPrescriptionController(DrugPrescriptionService drugPrescriptionService) {
        this.drugPrescriptionService = drugPrescriptionService;

    }

    /**
     * Obtiene todos los  prescripcion de medicamentos
     *
     * @return lista de  prescripcion de medicamentos
     */
    @GetMapping("/{patientId}")
    public ApiResponse<DrugPrescriptionDTOResponsePaginate> getAllDrugPrescription(@PathVariable BigInteger patientId, @RequestParam(value = "page", defaultValue = "0") int page,
                                                                                   @RequestParam(value = "size", defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(new Sort.Order(Sort.Direction.DESC, "id")));

        try {
            DrugPrescriptionDTOResponsePaginate drugPrescriptions = this.drugPrescriptionService.getPrescriptionnsByPatientPage(patientId, pageable);
            if (drugPrescriptions.getContent() != null) {
                return new ApiResponse<>(HttpStatus.OK.value(), MSG_FOUND, drugPrescriptions);
            } else {
                return new ApiResponse<>(HttpStatus.NOT_FOUND.value(), MSG_NOT_FOUND, drugPrescriptions);

            }
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, MSG_ERROR, e);
        }
    }

    /**
     * Guarda los prescripcion de medicamentos
     *
     * @param drugPrescriptionDTO dto que contiene informacion de la prescripcion de medicamento
     * @return informacion de prescripcion de medicamentos guardado
     */
    @PostMapping
    public ApiResponse<DrugPrescriptionDTO> saveDrugPrescription(
            @RequestBody DrugPrescriptionDTO drugPrescriptionDTO) {

        try {
            DrugPrescriptionDTO drugPrescriptionSaved = this.drugPrescriptionService.saveDrugPrescription(drugPrescriptionDTO);
            if (drugPrescriptionSaved != null) {
                return new ApiResponse<>(HttpStatus.OK.value(), MSG_SAVE, drugPrescriptionSaved);
            } else {
                return new ApiResponse<>(HttpStatus.NOT_FOUND.value(), MSG_ERROR_TRANS, null);

            }

        } catch (Exception e) {

            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, MSG_ERROR, e);

        }
    }

    @GetMapping("validateNumberPrescriptionByDrug/{patientId}/{drugId}")
    public ApiResponse<Integer> getValidateNumberPrescriptionsByDrug(@PathVariable BigInteger patientId, @PathVariable BigInteger drugId) {
        try {
            Integer valid = this.drugPrescriptionService.validatePrescriptionsDrugByPatient(patientId, drugId);

            return new ApiResponse<>(HttpStatus.OK.value(), MSG_FOUND, valid);

        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, MSG_ERROR, e);
        }
    }

}
