package com.saludtools.drugPrescription.controller;

import com.saludtools.drugPrescription.dto.PatientDTO;
import com.saludtools.drugPrescription.dto.PatientDTOResponsePaginate;
import com.saludtools.drugPrescription.dto.PatientInfoDTO;

import com.saludtools.drugPrescription.model.ApiResponse;
import com.saludtools.drugPrescription.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigInteger;

import static com.saludtools.drugPrescription.constants.General.*;

/**
 * Controlador de Pacientes
 *
 * @author kramos
 * @version 1
 * @since 06/10/2022
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/patient")
public class PatientController {

    private final PatientService patientService;

    /**
     * Constructor del controlador
     *
     * @param patientService servicio de pacientes
     */
    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;

    }

    /**
     * Obtiene todos los  pacientes
     *
     * @return lista de  pacientes
     */
    @GetMapping
    public ApiResponse<PatientDTOResponsePaginate> getAllPatient(@RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "size", defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(new Sort.Order(Sort.Direction.DESC, "id")));

        try {
            PatientDTOResponsePaginate patientsPaginate = this.patientService.getAllPatientsStatusActive(pageable);
            if (!patientsPaginate.getContent().isEmpty()) {
                return new ApiResponse<>(HttpStatus.OK.value(), MSG_FOUND, patientsPaginate);
            } else {
                return new ApiResponse<>(HttpStatus.NOT_FOUND.value(), MSG_NOT_FOUND, patientsPaginate);

            }
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, MSG_ERROR, e);
        }
    }

    /**
     * Guarda los pacientes
     *
     * @param patientDTO dto que contiene informacion de el paciente
     * @return informacion de pacientes guardado
     */
    @PostMapping
    public ApiResponse<PatientDTO> savePatient(
            @RequestBody PatientInfoDTO patientDTO) {

        try {
            PatientDTO patientSaved = this.patientService.savePatient(patientDTO);
            if (patientSaved != null) {
                return new ApiResponse<>(HttpStatus.OK.value(), MSG_SAVE, patientSaved);
            } else {
                return new ApiResponse<>(HttpStatus.NOT_FOUND.value(), MSG_ERROR_TRANS, null);

            }

        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, MSG_ERROR, e);

        }
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Boolean> deletePatient(@PathVariable BigInteger id) {
        try {

            if (this.patientService.deletePatient(id) == null) {
                return new ApiResponse<>(HttpStatus.NOT_FOUND.value(), MSG_NOT_FOUND_ID, false);
            } else {
                return new ApiResponse<>(HttpStatus.OK.value(), MSG_DELETE, true);

            }
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT.value(),
                    MSG_ERROR, e);
        }
    }

}
