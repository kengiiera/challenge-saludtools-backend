package com.saludtools.drugPrescription.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigInteger;
import java.util.Date;

/**
 * Clase de visualizacion pacientes
 *
 * @author kramos
 * @version 1
 * @since 22/01/2023
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PatientInfoDTO {

    private BigInteger id;
    private String name;
    private String surname;
    private String gender;
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date dateBirth;
    private Boolean status;

}