package com.saludtools.drugPrescription.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigInteger;
import java.util.Date;

/**
 * Clase de visualizacion pacientes
 *
 * @author kramos
 * @version 1
 * @since 22/01/2023
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PatientDTO {

    private BigInteger id;
    private String name;
    private String surname;
    private String gender;
    private int age;
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dateBirth;
    private Integer numberPrescription;
}