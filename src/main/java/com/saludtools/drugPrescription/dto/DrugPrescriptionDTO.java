package com.saludtools.drugPrescription.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.Date;

/**
 * Clase de visualizacion prescripcion de medicamentos
 *
 * @author kramos
 * @version 1
 * @since 22/01/2023
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DrugPrescriptionDTO {

    private BigInteger id;
    private BigInteger drugId;
    private PatientDTO patient;
    private String name;
    private Date dateInsert;


}
