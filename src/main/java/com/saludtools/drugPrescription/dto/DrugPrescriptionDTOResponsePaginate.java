package com.saludtools.drugPrescription.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Clase de visualizacion prescripcion de medicamentos paginado
 *
 * @author kramos
 * @version 1
 * @since 22/01/2023
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DrugPrescriptionDTOResponsePaginate {

    private List<DrugPrescriptionDTO> content;
    private Long totalElements;
    private int totalPages;

}
