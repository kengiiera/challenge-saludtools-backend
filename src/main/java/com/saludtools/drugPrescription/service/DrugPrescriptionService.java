package com.saludtools.drugPrescription.service;

import com.saludtools.drugPrescription.dto.DrugPrescriptionDTO;
import com.saludtools.drugPrescription.dto.DrugPrescriptionDTOResponsePaginate;
import com.saludtools.drugPrescription.entity.DrugPrescription;
import com.saludtools.drugPrescription.repository.DrugPrescriptionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Servicio para gestion prescripcion de medicamentos
 *
 * @author kramos
 * @version 1
 * @since 22/01/2023
 */
@Service
public class DrugPrescriptionService {
    @Autowired
    private DrugPrescriptionRepository drugPrescriptionRepository;
    @Autowired
    private ModelMapper modelMapper;


    /**
     * Guarda o actualiza una prescripcion de medicamento
     *
     * @param drugPrescriptionDTO dto que contiene informacion de prescripcion de medicamento
     * @return información de prescripcion de medicamento
     */
    @Transactional
    public DrugPrescriptionDTO saveDrugPrescription(DrugPrescriptionDTO drugPrescriptionDTO) {
        DrugPrescription drugPrescription = this.modelMapper.map(drugPrescriptionDTO, DrugPrescription.class);
        drugPrescription.setDateInsert(new Date());
        DrugPrescription drugPrescriptionSave = this.drugPrescriptionRepository.save(drugPrescription);

        return this.modelMapper.map(drugPrescriptionSave, DrugPrescriptionDTO.class);

    }


    /**
     * Validar numero de veces que ha sido prescritos un medicamentos  en el mes actual a un paciente
     *
     * @param patientId identificador del paciente
     * @param drugId    identificador del medicamento
     * @return 1 - numero de de veces que ha sido prescrito un medicamentos  en el mes actual a un paciente es 0
     * 0 - numero de de veces que ha sido prescrito un medicamentos  en el mes actual a un paciente es mayor o igual a 1
     */
    public Integer validatePrescriptionsDrugByPatient(BigInteger patientId, BigInteger drugId) {
        return this.drugPrescriptionRepository.getValidateIfExistPatientAndDrug(patientId, drugId);
    }

    /**
     * Obtener lista de prescripciones realizadas a un paciente
     *
     * @param patientId identificador del paciente
     * @return lista de  prescripciones por paciente
     */

    public DrugPrescriptionDTOResponsePaginate getPrescriptionnsByPatientPage(BigInteger patientId, Pageable page) {
        Page<DrugPrescription> drugPrescription = this.drugPrescriptionRepository.findByPatientId(patientId, page);
        DrugPrescriptionDTOResponsePaginate drugPrescriptionPaginate = new DrugPrescriptionDTOResponsePaginate();

        if (!drugPrescription.getContent().isEmpty()) {

            List<DrugPrescriptionDTO> list = new ArrayList<>();
            for (DrugPrescription drugPtn : drugPrescription.getContent()) {
                DrugPrescriptionDTO map = this.modelMapper.map(drugPtn, DrugPrescriptionDTO.class);
                list.add(map);
            }

            drugPrescriptionPaginate.setContent(list);

            drugPrescriptionPaginate.setTotalPages(drugPrescription.getTotalPages());
            drugPrescriptionPaginate.setTotalElements(drugPrescription.getTotalElements());

        }
        return drugPrescriptionPaginate;
    }

}
