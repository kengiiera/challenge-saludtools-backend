package com.saludtools.drugPrescription.service;

import com.saludtools.drugPrescription.dto.PatientDTO;
import com.saludtools.drugPrescription.dto.PatientDTOResponsePaginate;
import com.saludtools.drugPrescription.dto.PatientInfoDTO;
import com.saludtools.drugPrescription.entity.Patient;
import com.saludtools.drugPrescription.repository.DrugPrescriptionRepository;
import com.saludtools.drugPrescription.repository.PatientRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Servicio para gestion de pacientes
 *
 * @author kramos
 * @version 1
 * @since 06/10/2022
 */
@Service
public class PatientService {
    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private DrugPrescriptionRepository drugPrescriptionRepository;
    @Autowired
    private ModelMapper modelMapper;


    /**
     * Obtiene todos los pacientes
     *
     * @return lista de pacientes
     */
    public PatientDTOResponsePaginate getAllPatientsStatusActive(Pageable page) {
        Page<Patient> patients = this.patientRepository.findByStatus(true, page);

        PatientDTOResponsePaginate patientPaginate = new PatientDTOResponsePaginate();

        List<PatientDTO> list = new ArrayList<>();
        for (Patient patient : patients.getContent()) {

            list.add(convertTODTO(patient));
        }

        patientPaginate.setContent(list);
        patientPaginate.setTotalElements(patientPaginate.getTotalElements());
        patientPaginate.setTotalPages(patientPaginate.getTotalPages());


        return patientPaginate;
    }

    /**
     * Guarda o actualiza un paciente
     *
     * @param patientInfoDTO dto que contiene informacion de el tipo de
     *                       cita
     * @return información de paciente guardado
     */
    @Transactional
    public PatientDTO savePatient(PatientInfoDTO patientInfoDTO) {
        patientInfoDTO.setStatus(true);
        Patient patient = this.modelMapper.map(patientInfoDTO, Patient.class);
        Patient patientSaved = this.patientRepository.save(patient);
        return this.convertTODTO(patientSaved);
    }

    /**
     * Eliminado logico de un paciente
     *
     * @param id parametro de id de paciente
     * @return información de paciente inactivado
     */
    @Transactional
    public Patient deletePatient(BigInteger id) {
        Patient patient = new Patient();
        Optional<Patient> optionalPatient =this.patientRepository.findById(id);
        if (optionalPatient.isPresent()) {
            patient = optionalPatient.get();
            patient.setStatus(false);
            this.patientRepository.save(patient);
        }
        return patient;
    }

    public PatientDTO convertTODTO(Patient patient) {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        LocalDate dateBirth = LocalDate.parse((format.format(patient.getDateBirth())), fmt);
        LocalDate now = LocalDate.now();
        Period period = Period.between(dateBirth, now);
        PatientDTO map = modelMapper.map(patient, PatientDTO.class);
        map.setAge(period.getYears());
        map.setNumberPrescription(drugPrescriptionRepository.getValidateIfExistPrescriptions(patient.getId()));

        return map;
    }

}
