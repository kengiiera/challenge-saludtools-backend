package com.saludtools.drugPrescription.repository;

import com.saludtools.drugPrescription.entity.DrugPrescription;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

/**
 * Repositorio para prescripcion de medicamentos
 *
 * @author kramos
 * @version 1
 * @since 22/01/2023
 */
@Repository
public interface DrugPrescriptionRepository extends JpaRepository<DrugPrescription, BigInteger> {

    @Query(value = "select count(id) from prescripcion_medicamento  where  EXTRACT(MONTH FROM fecha_creacion) = (select  EXTRACT(MONTH FROM curdate())) and patient_id = :patientId and medicamento_id =:drugId", nativeQuery = true)
    Integer getValidateIfExistPatientAndDrug(BigInteger patientId, BigInteger drugId);

    @Query(value = "select  count(id)  from prescripcion_medicamento  where  EXTRACT(MONTH FROM fecha_creacion) = (select  EXTRACT(MONTH FROM curdate())) and patient_id = :patientId", nativeQuery = true)
    Integer getValidateIfExistPrescriptions(BigInteger patientId);

    Page<DrugPrescription> findByPatientId(BigInteger patientId, Pageable page);

}
