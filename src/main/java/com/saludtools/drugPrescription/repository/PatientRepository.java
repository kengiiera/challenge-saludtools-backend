package com.saludtools.drugPrescription.repository;


import com.saludtools.drugPrescription.entity.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;

/**
 * Repositorio para pacientes
 *
 * @author kramos
 * @version 1
 * @since 22/01/2023
 */
public interface PatientRepository extends JpaRepository<Patient, BigInteger> {

    Page<Patient> findByStatus(Boolean status, Pageable page);

}
