package com.saludtools.drugPrescription.Service;

import com.saludtools.drugPrescription.dto.PatientDTO;
import com.saludtools.drugPrescription.dto.PatientInfoDTO;
import com.saludtools.drugPrescription.entity.Patient;
import com.saludtools.drugPrescription.repository.DrugPrescriptionRepository;
import com.saludtools.drugPrescription.repository.PatientRepository;
import com.saludtools.drugPrescription.service.PatientService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;


@SpringBootTest
class PatientServiceTest {

    @Spy
    @InjectMocks
    private PatientService patientService;
    @Spy
    private ModelMapper modelMapper;
    @Mock
    PatientRepository patientRepository;
    @Mock
    DrugPrescriptionRepository drugPrescriptionRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void save_success_expect_dto() {
        // Arrange
        PatientInfoDTO patient = new PatientInfoDTO();
        patient.setGender("TEST");
        patient.setSurname("TEST");
        patient.setName("TEST");
        patient.setDateBirth(new Date("23/04/2010"));
        patient.setId(BigInteger.ONE);
        Patient patientSaved = new Patient();
        patientSaved.setGender("TEST");
        patientSaved.setSurname("TEST");
        patientSaved.setName("TEST");
        patientSaved.setDateBirth(patient.getDateBirth());
        patientSaved.setStatus(true);
        patientSaved.setId(BigInteger.ONE);

        Patient patientReturn = new Patient();
        patientReturn.setGender("TEST");
        patientReturn.setSurname("TEST");
        patientReturn.setName("TEST");
        patientReturn.setDateBirth(patient.getDateBirth());
        patientReturn.setStatus(true);
        PatientDTO patientDTO = new PatientDTO();
        patientDTO.setGender("TEST");
        patientDTO.setSurname("TEST");
        patientDTO.setName("TEST");
        patientDTO.setId(BigInteger.ONE);
        when(modelMapper.map(patient, Patient.class)).thenReturn(patientReturn);
        when(patientRepository.save(patientReturn)).thenReturn(patientSaved);
        when(modelMapper.map(patientSaved, PatientDTO.class)).thenReturn(patientDTO);
        when(drugPrescriptionRepository.getValidateIfExistPrescriptions(patient.getId())).thenReturn(1);

        // Act
        this.patientService.savePatient(patient);

        // Assert
        Mockito.verify(drugPrescriptionRepository, Mockito.times(1)).getValidateIfExistPrescriptions(patient.getId());
        Mockito.verify(patientRepository, Mockito.times(1)).save(patientReturn);

    }

    @Test
    void delete_success_expect_dto() {
        // Arrange
        Patient patient = new Patient();
        patient.setId(BigInteger.ONE);
        patient.setGender("TEST");
        patient.setSurname("TEST");
        patient.setName("TEST");
        patient.setDateBirth(new Date("23/04/2010"));
        Patient patientReturn = new Patient();
        patientReturn.setId(BigInteger.ONE);
        patientReturn.setGender("TEST");
        patientReturn.setSurname("TEST");
        patientReturn.setName("TEST");
        patientReturn.setDateBirth(patient.getDateBirth());
        patientReturn.setStatus(false);
        PatientDTO patientDTO = new PatientDTO();
        patientDTO.setGender("TEST");
        patientDTO.setSurname("TEST");
        patientDTO.setName("TEST");
        patientDTO.setId(BigInteger.ONE);
        Optional<Patient> patientOptional = Optional.of(patient);

        when(patientRepository.findById(patient.getId())).thenReturn(patientOptional);
        when(patientRepository.save(patient)).thenReturn(patientReturn);

        // Act
        this.patientService.deletePatient(patient.getId());
        LocalDate currentLocalDate = LocalDate.of(2029, 7, 13);


        // Assert
        Mockito.verify(patientRepository, Mockito.times(1)).findById(patient.getId());
        Mockito.verify(patientRepository, Mockito.times(1)).save(patient);

    }

    @Test
    void get_patient_success_expect_dto_page() {
        // Arrange

        Patient patient = new Patient();
        patient.setGender("TEST");
        patient.setSurname("TEST");
        patient.setName("TEST");
        patient.setDateBirth(new Date("23/04/2010"));
        PatientDTO patientDTO = new PatientDTO();
        patientDTO.setGender("TEST");
        patientDTO.setSurname("TEST");
        patientDTO.setName("TEST");
        patientDTO.setDateBirth(new Date("23/04/2010"));

        List<Patient> patientList = new ArrayList<>();
        patientList.add(patient);
        Pageable pageable = PageRequest.of(0, 5);
        Page<Patient> patientsPage = new PageImpl<>(patientList, pageable, 1);

        when(patientRepository.findByStatus(Boolean.TRUE, pageable)).thenReturn(patientsPage);
        when(modelMapper.map(patient, PatientDTO.class)).thenReturn(patientDTO);
        when(drugPrescriptionRepository.getValidateIfExistPrescriptions(patient.getId())).thenReturn(1);

        // Act
        this.patientService.getAllPatientsStatusActive(pageable);

        // Assert
        Mockito.verify(patientRepository, Mockito.times(1)).findByStatus(Boolean.TRUE, pageable);
        Mockito.verify(patientService, Mockito.times(1)).getAllPatientsStatusActive(pageable);

    }

}
