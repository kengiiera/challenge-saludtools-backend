package com.saludtools.drugPrescription.Service;

import com.saludtools.drugPrescription.dto.DrugPrescriptionDTO;
import com.saludtools.drugPrescription.dto.PatientDTO;
import com.saludtools.drugPrescription.entity.DrugPrescription;
import com.saludtools.drugPrescription.entity.Patient;
import com.saludtools.drugPrescription.repository.DrugPrescriptionRepository;
import com.saludtools.drugPrescription.service.DrugPrescriptionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
class DrugPrescriptionServiceTest {

    @Spy
    @InjectMocks
    private DrugPrescriptionService drugPrescriptionService;
    @Spy
    private ModelMapper modelMapper;
    @Mock
    DrugPrescriptionRepository drugPrescriptionRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

    }

    @Test
    void get_drug_prescription_by_patient_success_expect_dto_page() {
        // Arrange

        DrugPrescription drugPrescription = new DrugPrescription();
        Patient patient = new Patient();
        patient.setGender("TEST");
        patient.setSurname("TEST");
        patient.setName("TEST");
        patient.setDateBirth(new Date("23/04/2010"));
        drugPrescription.setName("Test");
        drugPrescription.setPatient(patient);
        drugPrescription.setDateInsert(new Date());
        DrugPrescriptionDTO drugPrescriptionDTO = new DrugPrescriptionDTO();
        drugPrescriptionDTO.setName("TEST");
        drugPrescriptionDTO.setDateInsert(new Date());
        List<DrugPrescription> drugList = new ArrayList<>();
        drugList.add(drugPrescription);
        Pageable pageable = PageRequest.of(0, 5);
        Page<DrugPrescription> drugPrescriptionPage = new PageImpl<>(drugList, pageable, 1);

        when(drugPrescriptionRepository.findByPatientId(patient.getId(), pageable)).thenReturn(drugPrescriptionPage);
        when(modelMapper.map(drugPrescription, DrugPrescriptionDTO.class)).thenReturn(drugPrescriptionDTO);

        // Act
        this.drugPrescriptionService.getPrescriptionnsByPatientPage(patient.getId(), pageable);

        // Assert
        Mockito.verify(drugPrescriptionRepository, Mockito.times(1)).findByPatientId(patient.getId(), pageable);
        Mockito.verify(drugPrescriptionService, Mockito.times(1)).getPrescriptionnsByPatientPage(patient.getId(), pageable);

    }


    @Test
    void get_drug_prescription_by_patient_empty_expect_dto_page() {
        // Arrange

        DrugPrescription drugPrescription = new DrugPrescription();
        Patient patient = new Patient();
        patient.setGender("TEST");
        patient.setSurname("TEST");
        patient.setName("TEST");
        patient.setDateBirth(new Date("23/04/2010"));
        drugPrescription.setName("Test");
        drugPrescription.setPatient(patient);
        drugPrescription.setDateInsert(new Date());
        DrugPrescriptionDTO drugPrescriptionDTO = new DrugPrescriptionDTO();
        drugPrescriptionDTO.setName("TEST");
        drugPrescriptionDTO.setDateInsert(new Date());
        Pageable pageable = PageRequest.of(0, 5);
        Page<DrugPrescription> drugPrescriptionPage = Mockito.mock(Page.class);

        when(drugPrescriptionRepository.findByPatientId(patient.getId(), pageable)).thenReturn(drugPrescriptionPage);

        // Act
        this.drugPrescriptionService.getPrescriptionnsByPatientPage(patient.getId(), pageable);

        // Assert
        Mockito.verify(drugPrescriptionService, Mockito.times(1)).getPrescriptionnsByPatientPage(patient.getId(), pageable);

    }

    @Test
    void save_success_expect_dto() {
        // Arrange
        DrugPrescription drugPrescription = new DrugPrescription();
        Patient patientSave= new Patient();
        patientSave.setGender("TEST");
        patientSave.setSurname("TEST");
        patientSave.setName("TEST");
        patientSave.setDateBirth(new Date("23/04/2010"));
        drugPrescription.setName("Test");
        drugPrescription.setPatient(patientSave);
        PatientDTO patient = new PatientDTO();
        patient.setGender("TEST");
        patient.setSurname("TEST");
        patient.setName("TEST");
        patient.setDateBirth(new Date("23/04/2010"));

        DrugPrescriptionDTO drugPrescriptionDTO = new DrugPrescriptionDTO();
        drugPrescriptionDTO.setName("TEST");
        drugPrescriptionDTO.setDateInsert(new Date());
        drugPrescriptionDTO.setPatient(patient);
        when(drugPrescriptionRepository.save(any())).thenReturn(drugPrescription);
        when(modelMapper.map(drugPrescriptionDTO, DrugPrescription.class)).thenReturn(drugPrescription);

        when(modelMapper.map(drugPrescription, DrugPrescriptionDTO.class)).thenReturn(drugPrescriptionDTO);

        // Act
        this.drugPrescriptionService.saveDrugPrescription(drugPrescriptionDTO);


        // Assert
        Mockito.verify(drugPrescriptionRepository, Mockito.times(1)).save(drugPrescription);
        Mockito.verify(drugPrescriptionService, Mockito.times(1)).saveDrugPrescription(drugPrescriptionDTO);

    }

    @Test
    void validate_if_have_prescriptions_success_expect_number_prescription() {
        // Arrange

        DrugPrescription drugPrescription = new DrugPrescription();
        Patient patient = new Patient();
        patient.setId(BigInteger.ONE);
        patient.setGender("TEST");
        patient.setSurname("TEST");
        patient.setName("TEST");
        patient.setDateBirth(new Date("23/04/2010"));
        drugPrescription.setName("Test");
        drugPrescription.setPatient(patient);
        drugPrescription.setDrugId(BigInteger.ONE);
        when(drugPrescriptionRepository.getValidateIfExistPatientAndDrug(patient.getId(), drugPrescription.getId())).thenReturn(1);

        // Act
        this.drugPrescriptionService.validatePrescriptionsDrugByPatient(patient.getId(), drugPrescription.getId());

        // Assert
        Mockito.verify(drugPrescriptionRepository, Mockito.times(1)).getValidateIfExistPatientAndDrug(patient.getId(), drugPrescription.getId());
        Mockito.verify(drugPrescriptionService, Mockito.times(1)).validatePrescriptionsDrugByPatient(patient.getId(), drugPrescription.getId());

    }
}
